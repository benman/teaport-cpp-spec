#!/usr/bin/env bash
# cd here
cd "$(dirname "$0")"

MODULE_NAME=cpp_httplib

install() {
    version=$1
    version_str="${version/./-}"
    url=$2
    version_file=$version/$MODULE_NAME.teaspec

    if [[ ! -f "$version_file" ]]; then
        echo "  writing $version_file"
        template='{
    "name": "cpp_httplib",
    "version": "VERSION",
    "summary": "A C++11 single-file header-only cross platform HTTP/HTTPS library.",
    "description": "long description",
    "homepage": "https://github.com/yhirose/cpp-httplib",
    "license": {
        "type": "MIT",
        "text": ""
    },
    "authors": {
        "Your Name": "yourl@email.com"
    },
    "artifact": {
        "archive": "URL",
        "remove_nests": true
    },
    "constraints": [
    ],
    "dependencies": {
    },
    "file_map": {
        "/.*\\.h": "/",
        "/CMakeLists\\.txt": "/",
        "/LICENSE": "/",
        "/README.md": "/"
    },
    "exclude_files": [
    ]
}

'
        template="${template/VERSION/$version}"
        template="${template/URL/$url}"
        if [[ ! -d $version ]]; then
            mkdir $version
        fi
        echo "$template" > "$version_file"
    fi

}

url_list=$(teaport scan_urls 'https://github.com/yhirose/cpp-httplib/releases')

for url in $url_list
do
    if [[ "$url" =~ archive/refs/tags/v(.*).zip$ ]]; then
        version=${BASH_REMATCH[1]}
        install "$version" "$url"
    fi
done