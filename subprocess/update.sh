#!/usr/bin/env bash
# cd here
cd "$(dirname "$0")"

install() {
    version=$1
    version_str="${version/./-}"
    url=$2
    version_file=$version/subprocess.teaspec

    if [[ ! -f "$version_file" ]]; then
        echo "  writing $version_file"
        template='{
    "name": "subprocess",
    "version": "VERSION",
    "summary": "cross platform modern c++ subprocess library",
    "description": "",
    "homepage": "https://github.com/benman64/subprocess.git",
    "license": {
        "type": "MIT",
        "text": ""
    },
    "artifact": {
        "archive": "URL",
        "remove_nests": true
    },
    "constraints": [
    ],
    "dependencies": {
    },
    "dir_map": {
    },
    "file_map": {
        "/src/cpp/*/.*": "/"
    },
    "exclude_files": [
    ]
}

'
        template="${template/VERSION/$version}"
        template="${template/URL/$url}"
        if [[ ! -d $version ]]; then
            mkdir $version
        fi
        echo "$template" > "$version_file"
    fi

}

url_list=$(teaport scan_urls 'https://github.com/benman64/subprocess/releases/')

for url in $url_list
do
    if [[ "$url" =~ archive/refs/tags/v(.*).zip$ ]]; then
        version=${BASH_REMATCH[1]}
        install "$version" "$url"
    fi
done