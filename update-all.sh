#!/usr/bin/env bash
# cd here
cd "$(dirname "$0")"

scripts=$(find ./ | grep 'update.sh$')

for script in $scripts
do
    echo "> $script"
    $script
done

git add .

echo "linting ..."
files="$(git diff --name-only HEAD | grep .teaspec$)"
IFS=$'\n'
for spec in $files; do
    if [[ -f $spec ]]; then
        echo "linting $spec"
        teaport lint $spec
    fi
done
unset IFS
