#!/usr/bin/env bash
# cd here
cd "$(dirname "$0")"

url_list=$(teaport scan_urls 'https://github.com/nlohmann/json/releases' | grep "download/v" | grep '/json.hpp$')

install() {
    version=$1
    version_str="${version/./-}"
    url=$2
    version_file=$version/json-$version_str.teaspec

    if [[ ! -f "$version_file" ]]; then
        echo "  writing $version_file"
        template='{
    "name": "nlohmann_json",
    "version": "VERSION",
    "summary": "JSON for Modern C++",
    "description": "long description",
    "homepage": "https://github.com/nlohmann/json",
    "license": {
        "type": "MIT",
        "text": ""
    },
    "authors": {
        "Your Name": "yourl@email.com"
    },
    "artifact": {
        "archive": "URL",
        "extract": false
    },
    "constraints": [
    ],
    "dependencies": {
    },
    "dir_map": {
        "/" : "/nlohmann"
    },
    "file_map": {
    },
    "exclude_files": [
    ]
}
'
        template="${template/VERSION/$version}"
        template="${template/URL/$url}"
        if [[ ! -d $version ]]; then
            mkdir $version
        fi
        echo "$template" > "$version_file"
    fi

}
for url in $url_list
do
    if [[ "$url" =~ /download/v(.*)/json.hpp ]]; then
        version=${BASH_REMATCH[1]}
        install "$version" "$url"
    fi
done